const data = (state = { loginUser: {} }, action) => {



  switch (action.type) {
    case "DATA":
      return {
        loginUser: action.payload
      }
    default:
      return state;
  }
}

export default data;