import data from './reducer'
import signData from './ReducerSignup';
import { combineReducers } from "redux";
import productdata from './ReducerProduct';
import productdata2 from './ReducerProduct2';
import billAmountdata from './reducerbillAmount';
const rootReducer = combineReducers({
 
  data,
  signData,
  productdata,
  productdata2,
  billAmountdata

});

export default rootReducer;