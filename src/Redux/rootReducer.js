import data from './Reducers/reducer'
import signData from './Reducers/ReducerSignup';
import { combineReducers } from "redux";
import productdata from './Reducers/ReducerProduct';
import productdata2 from './Reducers/ReducerProduct2';
import billAmountdata from './Reducers/reducerbillAmount';
const rootReducer = combineReducers({
  data,
  signData,
  productdata,
  productdata2,
  billAmountdata

});

export default rootReducer;