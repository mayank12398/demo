import { TextField } from '@material-ui/core'
import React from "react";
import { useState } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { useParams } from 'react-router-dom';
export default function CategoryFo(props) {
    const [inputs, setInputs] = useState({})
    const [signUp, setSignUp] = useState({ cheak : "demo" })
    var id = useParams()
    const handleChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        setInputs(values => ({ ...values, [name]: value }))
    }
    const Submit = () => {
        console.log("input",id)
        setSignUp(inputs)
        axios.put(`http://localhost:4900/Category/UpdateCategoryById/${id.id}`,{categoryName:inputs.categoryName}).then ((response)=> {
        })
    }

    return (
        <div className="catform">
            <div className="catforchild">
                <h3 className="adddcat ">upadate  category</h3>
                <br /> <br />
                <TextField
                    required
                    className="texttt"
                    placeholder=" name"
                    label="categoryName"
                    variant="outlined"
                    name="categoryName"
                    onChange={handleChange}
                    value={inputs.categoryName}
                />
                <br /> <br />
                <button onClick={Submit} className="addcat"> submit</button>
            </div>
        </div>
    )
}