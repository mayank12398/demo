import { TextField } from '@material-ui/core'
import React from "react";
import { useState, useEffect } from 'react';
import axios from 'axios';
import { useParams } from 'react-router-dom';
import toastr from 'toastr';
import 'toastr/build/toastr.min.css';
export default function AddNewProduct(props) {
    const formData = new FormData();
    var i = 0
    const [inputs, setInputs] = useState({})
    const [signUp, setSignUp] = useState({ cheak: "demo" })
    const [category, setCategory] = useState({})
    const [subcategory, setSubategory] = useState({})
    const [subcategorycmp, setSubategorycmp] = useState({})
    var id = useParams()
    const successMessage = () => {
        toastr.options = {
            positionClass: 'toast-top-full-width',
            hideDuration: 300,
            timeOut: 6000,
        };
        toastr.clear();
        setTimeout(() => toastr.success(`product added  Successfully`), 1000);
    }
    const errorMessage = () => {
        toastr.options = {
            positionClass: 'toast-top-full-width',
            hideDuration: 300,
            timeOut: 6000,
        };
        toastr.clear();
        setTimeout(() => toastr.error(` faild please cheak your detail `), 1000);
    }
    const handleChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        const d = new Date();
        let dateOfEntry = d.toISOString();
        // let img = img
        const status = true
        setInputs(values => ({ ...values, [name]: value, status, dateOfEntry }))
    }
    const handleChangecat = (e) => {
        let productCategory = parseInt(e.target.value)
        setInputs(values => ({ ...values, productCategory }))
        let arr;
        arr = subcategory.filter((val) => {
            if (productCategory == val.catId) {
                return val
            }
        })
        setSubategorycmp(arr);
    }
    const handleChangesubcat = (e) => {
        var productSubCategory = parseInt(e.target.value)
        console.log("subcartit", e.target.value)
        setInputs(values => ({ ...values, productSubCategory }))
    }
    const handleChangepriz = (e) => {
        let productPrice = parseInt(e.target.value)
        setInputs(values => ({ ...values, productPrice }))
    }
    const imgupload = (e) => {
        let file = (e.target.files[0])
        console.log("image", file)
        setInputs(values => ({ ...values, file }))
    }
    useEffect(() => {
        axios.get("http://localhost:4900/productCategory/get").then((response) => {
            setCategory(response.data.data)
        })
        axios.get("http://localhost:4900/SubCategory/GetSubCategoryList").then((response) => {
            setSubategory(response.data.data)
        })
    }, [])
    const Submit = (e) => {
        e.preventDefault()
        formData.append("productName", inputs.productName)
        formData.append("productCategory", inputs.productCategory)
        formData.append("productSubCategory", inputs.productSubCategory)
        formData.append("productPrice", inputs.productPrice)
        formData.append("productDescription", inputs.productDescription)
        formData.append("dateOfEntry", inputs.dateOfEntry)
        formData.append("status", inputs.status)
        formData.append("avatar", inputs.file)
        //setSignUp(inputs)


        axios({
            method:'POST',
            url:'http://localhost:4900/Product/AddProduct',
            data:formData,
            // headers: { "Content-Type": "multipart/form-data" }
        }).then(function(response){
            console.log("response",response)
            successMessage()
        }).catch((error) =>{
           //Network error comes in  
           errorMessage()
        });



     
    //  console.log("formData",formData.values[0])
    // for (var value of formData.values()) {
    //     console.log(value);
    //  }
    
    }
    return (
        <div className="catform">
            <div className="catforchild">
                <h3 className="adddcat ">  Add New product</h3>
                <br /> <br />
                <form onSubmit={Submit} action="/"  method='post' enctype="multipart/form-data">
                    <TextField
                        className="texttt vvvv"
                        placeholder=" productName"
                        label="productName"
                        variant="outlined"
                        name="productName"
                        onChange={handleChange}
                        value={inputs.productName}
                    />
                  
                    <br /> <br />
                    <TextField
                        className="texttt"
                        placeholder=" productDescription"
                        label="productDescription"
                        variant="outlined"
                        name="productDescription"
                        onChange={handleChange}
                        value={inputs.productDescription}
                    />

                    <br /> <br />

                    <TextField
                        className="texttt"
                        placeholder=" productPrice"
                        label="productPrice"
                        variant="outlined"
                        name="productPrice"
                        onChange={handleChangepriz}
                        value={inputs.productPrice}
                    />
                    <br /> <br />

                    <select className="dropdownn " onChange={handleChangecat} >
                        {console.log("cpppppppp", subcategorycmp)}
                        <option> --Select Category-- </option>
                        {category.length > 0 && category.map((val) => {
                            return (<option className="item1" name="catId" value={`${val.catId}`} >
                                {val.categoryName}
                            </option>
                            )
                        })}

                    </select>

                    <select className="dropdownn " onChange={handleChangesubcat}  >
                        <option> --Select Sub categoryCategory-- </option>

                        {subcategorycmp.length > 0 && subcategorycmp.map((val) => {
                            return <>
                                {(<option className="item1" value={`${val.subCatId}`} >
                                    {val.subCategoryName}

                                </option>
                                )}
                            </>
                        })}

                    </select>
                    <input className='productImagepost' type='file' name="avatar" onChange={imgupload} />
                    <input type="submit" className="addcatt" value='submit' />
                </form>
            </div>



        </div>
    )
}