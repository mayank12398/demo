import axios from "axios";
import React, { useState } from "react";
import { Card, Container } from "@material-ui/core";
import { Link } from "react-router-dom";
import { Table, TableBody, TableCell, TableHead, TableRow } from '@material-ui/core';
import { useEffect } from "react";
export default function ProductApi() {
    const [product, setProduct] = useState({})
    const Getproduct = () => {
        axios.get("http://localhost:4900/Product/GetProductList").then((response) => {
            setProduct(response.data.data)
        })
    }

    useEffect(() => {
        Getproduct();
    }, [])

    const handleOpen = (id) => {

    }
    // const handleDelete = (id) => {
    //     var index = product.findIndex((val) => val.productId == id)
    //     var x= product.splice(index, 1)
       
    //      setProduct(product)
        
    // }

   

    return (<>
        <div className="categry">
            <div>

                <span className="addcatname"> Product  </span>


                <Link to="/ProductsForm" className="addprodbutt">   <buton className=""> Add Product </buton> </Link>
            </div>
            <Container className="productApis" >
                <Card sx={{ mt: 2, mb: 2 }} className="explore">   <Table className="categoryTable">
                    <TableHead className="tablehead" >
                        <TableRow  >

                            <TableCell className="tabletext">productId </TableCell>
                            <TableCell className="tabletext">productName </TableCell>
                            <TableCell className="tabletext">productCategory </TableCell>
                            <TableCell className="tabletext">productSubCategory</TableCell>
                            <TableCell className="tabletext">productDescription</TableCell>
                            <TableCell className="tabletext">productPrice</TableCell>
                            {/* <TableCell className="tabletext">dateOfEntry</TableCell> */}
                            <TableCell className="tabletext">Update</TableCell>
                           



                        </TableRow>
                    </TableHead >
                    <TableBody>
                        {product.length > 0 && product.map((val, id) => (<TableRow key={id} component="th" scope="row" >
                            <TableCell className="tabletextt" >{val.productId}</TableCell>
                            <TableCell className="tabletextt" >{val.productName}</TableCell>
                            <TableCell className="tabletextt" >{val.productCategory}</TableCell>
                            {/* {console.log("product",product)} */}
                            <TableCell className="tabletextt" >{val.productSubCategory}</TableCell>
                            <TableCell className="tabletextt" >{val.productDescription}</TableCell>
                            <TableCell className="tabletextt" >{val.productPrice}</TableCell>
                            {/* <TableCell className="tabletextt" >{val.dateOfEntry}</TableCell> */}
                            <TableCell className="tabletextt" > <Link to={`/UpdateProductItem/${val.productId}`}  >  <button className="catbuttonn " onClick={() => { handleOpen(id); }} > Edit</button> </Link>  </TableCell>
                           


                        </TableRow>))}
                    </TableBody>
                </Table>    </Card>  </Container>

        </div>
    </>)
}