import { TextField } from '@material-ui/core'
import React from "react";
import { useState } from 'react';
import axios from 'axios';
export default function AddnewSubCategoryFo() {
    const [inputs, setInputs] = useState({})
    const [signUp, setSignUp] = useState({ cheak: "demo" })
    const handleChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        setInputs(values => ({ ...values, [name]: value }))
    }
    const handleChangea = (event) => {

        const name = event.target.name;
        const value = parseInt(event.target.value)

        setInputs(values => ({ ...values, [name]: value }))
    }
    const Submit = () => {
        setSignUp(inputs)
        axios.post("http://localhost:4900/SubCategory/AddSubCategory", inputs).then(function (response) {
            console.log("response",response)
        })
    }
    return (
        <div className="catform">
            <div className=" catforchild">
                <h3 className="adddcat ">Add New Subcategory</h3>

                <TextField
                    required
                    className="texttt"
                    placeholder=" name"
                    label="subCategoryName"
                    variant="outlined"
                    name="subCategoryName"
                    onChange={handleChange}
                    value={inputs.subCategoryName}
                />
  <br /> <br />
               
                <TextField
                    required
                    className="texttt"
                    placeholder=" catId"
                    label="catId"
                    variant="outlined"
                    name="catId"
                    onChange={handleChangea}
                    value={inputs.catId}
                />
                <br /> <br />
                <button onClick={Submit} className="addcat"> Add</button>


            </div>



        </div>
    )
}