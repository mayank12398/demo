import { TextField } from '@material-ui/core'
import React from "react";
import { useState } from 'react';
import { useParams } from 'react-router-dom';
import axios from 'axios';
export default function UpdateProductItem() {
    var id = useParams()
    const [inputs, setInputs] = useState({})
    const [signUp, setSignUp] = useState({ cheak: "demo" })
    const handleChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        setInputs(values => ({ ...values, [name]: value }))
    }
    const handleChangea = (event) => {
        const name = event.target.name;
        const value = parseInt(event.target.value)
        setInputs(values => ({ ...values, [name]: value }))
    }
    const Submit = () => {
        console.log("ffff",inputs)
        setSignUp(inputs)
        axios.put(`http://localhost:4900/product/updateProduct/${id.id}`, {productName:inputs.productName,productCategory:inputs.productCategory,productSubCategory:inputs.productSubCategory,productDescription:inputs.productDescription,productPrice:inputs.productPrice} ).then(function (response) {
            console.log("response", response)
        })
    }
    return (
        <div className="catform">
            <div className=" catforchild">
                <h3 className="adddcat ">Update product detail</h3>
                <TextField
                    required
                    className="texttt"
                    label="productName"
                    variant="outlined"
                    name="productName"
                    onChange={handleChange}
                    value={inputs.productName}
                />
                <br /> <br />

                <TextField
                    required
                    className="texttt"
                    placeholder=" name"
                    label="productCategory"
                    variant="outlined"
                    name="productCategory"
                    onChange={handleChange}
                    value={inputs.productCategory}
                />
                <br /> <br />

                <TextField
                    required
                    className="texttt"
                    placeholder=" name"
                    label="productSubCategory"
                    variant="outlined"
                    name="productSubCategory"
                    onChange={handleChange}
                    value={inputs.productSubCategory}
                />
                <br /> <br />

                <TextField
                    required
                    className="texttt"
                    placeholder=" name"
                    label="productDescription"
                    variant="outlined"
                    name="productDescription"
                    onChange={handleChange}
                    value={inputs.productDescription}
                />
                <br /> <br />
                <TextField
                    required
                    className="texttt"
                    placeholder=" productPrice"
                    label="productPrice"
                    variant="outlined"
                    name="productPrice"
                    onChange={handleChangea}
                    value={inputs.productPrice}
                />
                <br /> <br />
                <button onClick={Submit} className="addcat"> Update</button>
            </div>
        </div>
    )
}