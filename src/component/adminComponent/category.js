import React, { useState } from "react";
import { Link } from "react-router-dom";
import { useEffect } from "react";
import axios from "axios";

import { Card, Container } from "@material-ui/core";
import { Table, TableBody, TableCell, TableHead, TableRow } from '@material-ui/core';

export default function Category() {


    const [category, setCategory] = useState({})

    const GetCategory = () => {
        axios.get("http://localhost:4900/productCategory/get").then((response) => {
            setCategory(response.data.data)

        })

    }
    useEffect(() => {

        GetCategory()


    }, [])

    const handleOpen = (i) => {
        console.log("categoryssssssssssssssssssssssssssssssssssssssssssss" , category[i])
    }
    return (
        <>
            <div className="categry" >
            <Container className="categorycon" >
                    <Card sx={{ mt: 2, mb: 2 }} className="explore"> 
                <div className="categorydiv">

                    <span className="addcatname"> Category  </span>
                   
                    
                 <Link to="/addnewcategoryForm" className="adnewctr">   <buton className="addcatbutt"> Add New category </buton> </Link> 
                </div>
                <Table className="categoryTable">
                    <TableHead className="tablehead" >
                            <TableRow  >
                                <TableCell className="tabletext">Category ID</TableCell>
                                <TableCell className="tabletext">Category Name</TableCell>
                                <TableCell className="tabletext">Edit button</TableCell>

                            </TableRow>
                        </TableHead >
                        <TableBody>
                            {category.length > 0 && category.map((val, id) => (<TableRow key={id} component="th" scope="row" >
                               
                                <TableCell className="tabletextt" >{val.catId}</TableCell>
                                <TableCell className="tabletextt" >{val.categoryName}</TableCell>

                                <TableCell className="tabletextt" > <Link to={`/categoryForm/${val.catId}`}  >  <button className="catbuttonn " onClick={() => { handleOpen(id); }} > Edit</button> </Link>  </TableCell>


                            </TableRow>))}
                        </TableBody>
                    </Table>    </Card>  </Container>
            </div>
        </>
    )
}