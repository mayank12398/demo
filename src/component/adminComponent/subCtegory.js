import React, { useState } from "react";
import { Button } from "reactstrap";
import { Link } from "react-router-dom";
import { useEffect } from "react";
import axios from "axios";
import { Card, Container } from "@material-ui/core";
import { Table, TableBody, TableCell, TableHead, TableRow } from '@material-ui/core';
export default function SubCategory() {
    const [subCategory, setSubCategory] = useState({})
    const GetCategory = () => {
        axios.get("http://localhost:4900/SubCategory/GetSubCategoryList").then((response) => {
            setSubCategory(response.data.data)   
        })
    }
    useEffect(() => {
        GetCategory()
    }, [])
    const handleOpen = (i) => {


        console.log("categorysss" , subCategory)

    }


    return (
        <>
            <div className="categry">
                
                <div>

                    <span className="addcatname"> SubCategory  </span>
                   
                    
                 <Link to="/addnewsubCategoryForm" className="adnewctr">   <buton className="addcatbutt"> Add SubCategory </buton> </Link> 
                </div>
                <Container className="subcategorys" >
                    <Card sx={{ mt: 2, mb: 2 }} className="explore">   <Table className="categoryTable">
                    <TableHead className="tablehead" >
                            <TableRow  >
                             
                                <TableCell className="tabletext">Subcategory Id</TableCell>
                                <TableCell className="tabletext">Subcategory Name</TableCell>
                                <TableCell className="tabletext">Category Id</TableCell>
                                <TableCell className="tabletext">Edit button</TableCell>

                            </TableRow>
                        </TableHead >
                        <TableBody>
                            {subCategory.length > 0 && subCategory.map((val, id) => (<TableRow key={id} component="th" scope="row" >
                                  
                                <TableCell className="tabletextt" >{val.subCatId}</TableCell>
                                <TableCell className="tabletextt" >{val.subCategoryName}</TableCell>
                                <TableCell className="tabletextt" >{val.catId}</TableCell>

                                <TableCell className="tabletextt" > <Link to={`/subCategoryForm/${id+1}`}  >  <button className="catbuttonn " onClick={() => { handleOpen(id); }} > Edit</button> </Link>  </TableCell>


                            </TableRow>))}
                        </TableBody>
                    </Table>    </Card>  </Container>






                {/* <Link to="/categoryForm">   <button className="catbutton" > click me </button> </Link> */}



            </div>
        </>
    )
}