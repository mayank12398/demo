import {TextField, Button } from '@material-ui/core'
import React from "react";
import { useState, useEffect } from 'react';
import { Radio, RadioGroup, FormControlLabel, FormControl, FormLabel } from '@mui/material';
import axios from 'axios';
import { Card, Container } from "@material-ui/core";
import { useNavigate } from 'react-router-dom';
import toastr from 'toastr';
import 'toastr/build/toastr.min.css';

export default function SignUp() {
    const [inputs, setInputs] = useState({})
    const [signUp, setSignUp] = useState({ cheak: "demo" })

    // const notify = () => toast("SignUp Successfully");

    // const history = useHistory();
    const navigate = useNavigate();


    const handleChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;

        setInputs(values => ({ ...values, [name]: value}))
    }
    
    // const redirect = ()=>{
    //     // navigate('/SignIn')

    //     console.log("demopurpse")
    // }
    const successMessage = ()=>{
        toastr.options = {
            positionClass: 'toast-top-full-width',
            hideDuration: 300,
            timeOut: 6000,
          };
          toastr.clear();
          setTimeout(() => toastr.success(`SignUp Successfully`), 1000);
    }

    const errorMessage = ()=>{
        toastr.options = {
            positionClass: 'toast-top-full-width',
            hideDuration: 300,
            timeOut: 6000,
          };
          toastr.clear();
          setTimeout(() => toastr.error(`SignUp faild please cheak your detail `), 1000);
    }
    
    const registerApi = (data) => {
        const baseURL = "http://localhost:4900/user/register";
        axios.post(baseURL, data).then((response) => {
            console.log("ssss",response)
           
            //   ((response.data.isSuccess == true ) ?  navigate('/ResistersuccessFully') : navigate('/SignUp'))
            //   ((response.data.isSuccess == true ) ? successMessage() :errorMessage())


            
              if(response.data.isSuccess){
                if((response.data.message==="Signup Successfully")){
                    successMessage()     
                        navigate('/SignIn')
                    
                }
            }
            else{
                errorMessage()
            }

    });
    }
    const SignUp = () => {
        console.log("inputs", inputs)
        registerApi(inputs);
        // axios.get('http://localhost:4900/users/get').then((response)=>{
        //     console.log("demo",response)
        // })
         setSignUp(inputs)
    }
   
    return (

<div className='signscrllbar'>  
        <Container className='signupcontainer' >    <Card sx={{ mt: 2, mb: 2 }} className="exploreSignup">

            <div className="  signupitem ">
            {/* <ToastContainer /> */}

                <h3 className=" ">Creat to New  account</h3>
                <TextField
                    required
                    className="textt"
                    placeholder=" name"
                    label="firstName"
                    variant="outlined"
                    name="firstName"
                    onChange={handleChange}
                    value={inputs.firstName}
 
                />  <br />  <br />


                {((signUp.cheak == "demo") ? " " : ((signUp.firstName == undefined) ? <span className="validatinn">  firstName can't be empty </span> : (typeof (signUp.firstName) != 'string') ? <span className="validatinn"> This firstName is not valid </span> : " " ))}
                <br /> <br /> 
                <TextField
                    required
                    className="textt"
                    placeholder="name"
                    label="lastName"
                    variant="outlined"
                    name="lastName"
                    onChange={handleChange}
                    value={inputs.lastName}
                /><br />  <br />

                {((signUp.cheak == "demo") ? " " : ((signUp.lastName == undefined) ? <span className="validatinn"> lastName can't be empty </span> : (typeof (signUp.lastName) != 'string') ? <span className="validatinn"> This lastName is not valid </span> : " "))}
                <br /> <br />

                <TextField
                    required
                    className="textt"
                    placeholder="userName"
                    label="userName"
                    variant="outlined"
                    name="userName"
                    onChange={handleChange}
                    value={inputs.userName}
                /><br />  <br />

                {((signUp.cheak == "demo") ? " " : ((signUp.userName == undefined) ? <span className="validatinn">username can't be empty </span> : ((signUp.userName.length) <= 5) ? <span className="validatinn"> This username is too sort </span> : " "))}

                <br /> <br />



                <TextField
                    required
                    className="textt"
                    id="outlined-password-input"
                    label="Password"
                    type="password"
                    autoComplete="current-password"
                    variant="outlined"
                    name="password"
                    onChange={handleChange}
                    value={inputs.password}
                /> <br /> <br />



                {((signUp.cheak == "demo") ? " " : ((signUp.password == undefined) ? <span className="validatinn">Password can't be empty </span> : ((/[A-Z]+[a-z0-9]*[\W]+[0-9]*/).test(inputs.password) != true) ? <span className="validatinnpass"> Password at least first uppercase and a special charecter required </span> : " "))}

                <br /> <br />

                <TextField
                    required
                    className="textt"
                    id="outlined-password-input"
                    label="confirm  Password"
                    type="password"
                    autoComplete="current-password"
                    variant="outlined"
                    name="confirmPassword"
                    onChange={handleChange}
                    value={inputs.confirmPassword}
                /> <br />  <br />

                {((signUp.confirmPassword) != (signUp.password)) ? <span className="validatinn"> Pasword did not match</span> : " "}
                <br /> <br />
                <TextField
                    required
                    className="textt"
                    placeholder="email"
                    label="email"
                    variant="outlined"
                    name="email"
                    onChange={handleChange}
                    value={inputs.email}
 
                /> <br />  <br />



                {((signUp.cheak == "demo") ? " " : ((signUp.email == undefined) ? <span className="validatinn">Email can't be empty </span> : ((/^[a-zA-Z0-9]+[@][a-z]+[\.][a-z]{2,3}/).test(inputs.email) != true) ? <span className="validatinn"> This email is  invalid </span> : " "))}
                <br /> <br /><br />
                <FormControl component="fieldset" className="radio">
                    <FormLabel component="legend">Gender</FormLabel>
                    <RadioGroup row aria-label="gender" name="gender">
                        <FormControlLabel value={"Female"} onChange={handleChange} control={<Radio />} label="Female" />
                        <FormControlLabel value={"Male"} onChange={handleChange} control={<Radio />} label="Male" />
                    </RadioGroup>
                </FormControl> 
                <br /><br /> 

                {/* <Link to="/" className="signuobutt"> </Link> */}
                <Button variant="contained" onClick={SignUp} className="signupbtn"> Sign Up</Button>   <br /><br />


            </div>
          

        </Card>   </Container>



        </div>
    )
}