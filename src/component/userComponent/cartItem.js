import React from "react";
import img1 from '../../assets/images/img1.jpg'
import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import ProductCartItem from "../../Redux/Actions/ActionitemData";
import Bill from "../../Redux/Actions/ActionbillAmount";
import { useNavigate } from 'react-router-dom';
import addCartItem from "../../Redux/Actions/ActinPrItem";
import toastr from 'toastr';
import 'toastr/build/toastr.min.css';
import { Container, Row, Col } from 'react-grid-system';
export default function CartItems() {
    var x = 0
    const dispatch = useDispatch()

    const navigate = useNavigate();

    const [imgData, setImgData] = useState({})


    const { idata, loinData } = useSelector((state) => {

     

        return ({
            idata: state.productdata.addToCart,
            loinData: state.data.loginUser
        });
    });

    const worningMessage = () => {
        toastr.options = {
            positionClass: 'toast-top-full-width',
            hideDuration: 300,
            timeOut: 8000,
        };
        toastr.clear();
        setTimeout(() => toastr.warning(`Please login before order `), 1000);
    }

    const worningMessageDicriment = () => {
        toastr.options = {
            positionClass: 'toast-top-full-width',
            hideDuration: 300,
            timeOut: 8000,
        };
        toastr.clear();
        setTimeout(() => toastr.warning(`You can't select less  than 1 product `), 1000);
    }

    const worningMessageIncriment = () => {
        toastr.options = {
            positionClass: 'toast-top-full-width',
            hideDuration: 300,
            timeOut: 8000,
        };
        toastr.clear();
        setTimeout(() => toastr.warning(`You can't select more than 10 product `), 1000);
    }
    useEffect(() => {
        setImgData(idata)
    }, [idata])

    const incriment = (id) => {
        var data = idata.map((value) => {
            if (value.productId == id) {
                return { ...value, productQuatuty: value.productQuatuty + 1 };
            }
            return value;
        })
        setImgData(data)
        dispatch(addCartItem(data))                                                      

        const Found = idata.find((value) => (value.productQuatuty == 9) &&  (value.productId==id) )
        if (Found) {
            worningMessageIncriment()
        }
    }
    const totelAmount = () => {
        ((loinData != null) ? navigate('/Paymentmode') : (worningMessage() ? navigate('/SignIn') : navigate('/cartItem')))
        dispatch(Bill(x))
    }


    const dicriment = (id) => {
        let Found
        var data = idata.map((value) => {
            if (value.productId == id) {
                return { ...value, productQuatuty: value.productQuatuty - 1 };
            }
            return value;

        })
        setImgData(data)
        dispatch(addCartItem(data))


        Found = idata.find((value) => ( (value.productQuatuty == 2) && (value.productId==id)))
        console.log("ddd",Found)
        if (Found) {
            worningMessageDicriment()
        }



    }

    const remove = (e) => {

        dispatch(ProductCartItem(idata[e]))

    }


    return (
        <div className="cart">
            
               <Container className='productContainer' >   
            <Row>
                
            <h3 className="shopinCart">Shoping Cart </h3>
            {(idata.length != 0) ? <> {idata.length > 0 && idata.map((val, i) => {
                { (idata[i].productPrice > 0) && (x += idata[i].productPrice * idata[i].productQuatuty) }
                return (

                    <>
                    <Col md={12} >   
                        {(val.productId != undefined) ? <>  <div className="cartitem">
                            <div>
                                <img className="productcartimage " src={val.productImageUrl} />
                            </div>
                            <div className="itemspace">
                                <h3 className="productName" >  Product Name</h3>
                                <h4 className="productNamed" > {val.productName.charAt(0).toUpperCase() + val.productName.slice(1)}</h4>
                            </div>
                            <div className="itemspace "> <h3 className="productName" > Items</h3>
                                {(val.productQuatuty <= 9) ? <button className="cartbutton " onClick={() => incriment(val.productId)} > + </button> : " "} <span className="productNamed prices"> {idata[i].productQuatuty} </span>{(val.productQuatuty >= 2 )  ? <button onClick={() => dicriment(val.productId)} className="cartbutton"> - </button> : " "}
                            </div>
                            <div className="itemspace"> <h3 className="productName">Prize </h3>

                                <span className="productNamed prices">{idata[i].productPrice * idata[i].productQuatuty}</span>



                                <span className="itemRemove" onClick={() => remove(i)} ><HighlightOffIcon className="cancelItem" /> </span>

                            </div>

                        </div> </> : " "}
                        </Col>
                    </>
                )

            })}
                <h3 className="totel"> Totel Payable Amount :- {x} ₹ </h3>
                <button className="payment" onClick={totelAmount}>  Order now </button>
            </> : <h1 className="cartempty"> Cart is empty  🛒  </h1>}
          
            </Row>
              </Container>
        </div>
    )
}