import React from "react";
import img1 from '../../assets/images/img1.jpg'
import { useSelector } from "react-redux";
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import { useDispatch } from "react-redux";
import Productname from "../../Redux/Actions/ActionProduct";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';
import { Link } from 'react-router-dom'
import { useState, useEffect } from 'react';
import PlaylistAddCheckCircleIcon from '@mui/icons-material/PlaylistAddCheckCircle';
export default function ShowProduct() {
    const dispatch = useDispatch()

    const [productLength, setProductLength] = useState(null)
    const { imgData, productData } = useSelector((state) => {

        return ({ imgData: state.productdata2.productdetail2, productData: state.productdata.addToCart });
    });


    useEffect(() => {

        setProductLength(productData.length)

    }, [])



    const AddItem = () => {
        dispatch(Productname(imgData))

    }
   

  const cartData = productData.find((data) => (data.productId) === imgData.productId);

    const AddItemcart = () => {

    }

    return (
        <>
            <div className="productshow">
                <Carousel className="productshowimage">
                    <div className="dddsss">
                        <img className="iamgecls" src={imgData.productImageUrl} />
                    </div>
                    <div>
                        <img className="iamgecls"src={imgData.productImageUrl}/>
                    </div>
                    <div>
                        <img className="iamgecls" src={imgData.productImageUrl} /> 
                    </div>
                </Carousel>
                <div className="productprizedetail">
                    <h3> Product detail </h3>
                    <h4>Product Name--{imgData.productName}</h4>
                    <h4> prize--{imgData.productPrice} </h4>
                    <h4>product Description--{imgData.productDescription}</h4>
                    {((productData.length > productLength) || (cartData)) ? <Link to="/cartItem">   <button className="productbuttnn" > <PlaylistAddCheckCircleIcon className="" /> <span className="addtocart"  >Go to cart</span></button>  </Link> : <button onClick={() => { AddItem() }} className="productbuttnn" > <ShoppingCartIcon className="" /> <span className="addtocart" >Add to cart</span></button>
                    }
                </div>
              
            </div>


        </>
    )
}