import React from "react";
import { PayPalButton } from "react-paypal-button-v2";
import { useSelector } from "react-redux";
import toastr from 'toastr';
export default function PaypalPayment() {
  const { totelAmount } = useSelector((state) => {

    return ({ totelAmount: state.billAmountdata.amount });
  });
  console.log("sssssss", totelAmount)

  const successMessage = (name) => {
    toastr.options = {
      positionClass: 'toast-top-full-width',
      hideDuration: 300,
      timeOut: 6000,
    };
    toastr.clear();
    setTimeout(() => toastr.success(`Transaction completed by ${name}`), 1000);
  }

  const errorMessage = () => {
    toastr.options = {
      positionClass: 'toast-top-full-width',
      hideDuration: 300,
      timeOut: 10000,
    };
    toastr.clear();
    setTimeout(() => toastr.error(`Login failed,Please check your username and password is correct.`), 1000);
  }

  return (
    <>
    <h2> Amount payble:-  {totelAmount} </h2>
      <PayPalButton
        amount={totelAmount}
        // shippingPreference="NO_SHIPPING" // default is "GET_FROM_FILE"
        onSuccess={(details, data) => {
          // alert("Transaction completed by " + details.payer.name.given_name);
          successMessage(details.payer.name.given_name)

          // OPTIONAL: Call your server to save the transaction
          return fetch("/paypal-transaction-complete", {
            method: "post",
            body: JSON.stringify({
              orderID: data.orderID
            })
          });
        }}
      />
    </>
  )
}