
import React from "react";
import img1 from '../../assets/images/img1.jpg'
import '../../assets/cssFile/webproduct.css'
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import axios from "axios";
import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Productname from "../../Redux/Actions/ActionProduct";
import Productname2 from "../../Redux/Actions/ActionProduct2";
import { Link } from "react-router-dom";
import { Container, Row, Col } from 'react-grid-system';
import PlaylistAddCheckCircleIcon from '@mui/icons-material/PlaylistAddCheckCircle';
export default function Product() {

    const dispatch = useDispatch()
    const [item, setItem] = useState({})


    useEffect(() => {

        axios.get("http://localhost:4900/Product/GetProductList").then((response) => {


            // response.data.data.map((value) => { value.productQuatuty = 1 })
            setItem(response.data.data)

        })
        // setItem(allProductData)

    }, [])



    const AddItem = (e) => {

        dispatch(Productname(item[e]))

    }

    const { productData } = useSelector(state => {
        return ({ productData: state.productdata.addToCart })
    })



    const showImage = (e) => {
        dispatch(Productname2(item[e]))
    }

    const AddItemcart = (e) => {
        dispatch(Productname2(item[e]))

    }

    return (
        <div className='aalllproduct'>


            <Container className='productContainer' >   
            <Row>
                    {item.length > 0 && item.map((val, i) => {

                        return (

                           <Col xs={12} sm={6} lg={4} xl={3}  md={4} className="productitems" > <Link to="/productshow" className=" " ><img className="productimage" onClick={() => { showImage(i) }} src={val.productImageUrl} /></Link> <p className="productItem" > {val.productName.charAt(0).toUpperCase() + val.productName.slice(1)}
                            </p> <p className="productprize">₹  {val.productPrice}  </p>

                                {(productData.length > 0) ? (productData.find((values) => {
                                    return values.productId == val.productId
                                })) === undefined ? <button className="productbuttn" onClick={() => { AddItem(i) }}  > <ShoppingCartIcon className="carticon" /> <span className="addtocart" >Add to cart</span></button> : <Link to="/cartItem">
                                    <button className="productbuttn" onClick={() => { AddItemcart(i) }} > <PlaylistAddCheckCircleIcon className="carticon" /> <span className="addtocart" >Go to cart</span></button> </Link> : <button className="productbuttn" onClick={() => { AddItem(i) }}  > <ShoppingCartIcon className="carticon" /> <span className="addtocart" >Add to cart</span></button>}

                            </Col>

                        )

                    })}

             



</Row>
              </Container>


        </div>
    )
}