import React from 'react'
import { TextField, Button } from '@material-ui/core'
import { Link } from "react-router-dom";
import { useState, useEffect } from 'react';
import axios from 'axios';
import Username from '../../Redux/Actions/Action';
// import Username from '../../Redux/Actions/Action';
import { useNavigate } from 'react-router-dom';
import { Card, Container } from "@material-ui/core";
import { useSelector, useDispatch } from "react-redux";
import toastr from 'toastr';
import 'toastr/build/toastr.min.css';
const Login = () => {
    const [inputs, setInputs] = useState({})
    const [loinData, setLoinData] = useState({ cheak: "demo" })
    const dispatch = useDispatch()
    const navigate = useNavigate();
    const handleChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        setInputs(values => ({ ...values, [name]: value }))
        setInputs(values => ({ ...values, rememberMe: true }))
       
    }
    const { name ,product } = useSelector((state) => {
        return ({ name: state.data.loginUser, product:state.productdata.addToCart});

    });

    const successMessage = ()=>{
        toastr.options = {
            positionClass: 'toast-top-full-width',
            hideDuration: 300,
            timeOut: 6000,
          };
          toastr.clear();
          setTimeout(() => toastr.success(`Login Successfully`), 1000);
    }

    const loginApi = (data) => {
        const baseURL = "http://localhost:4900/user/login"
        axios.post(baseURL, data).then((response) => {
        console.log ( "ssssssssss",response)
        // if(response.data.isSuccess){

    //    console.log( "jkhfdkhajkds",response.data.data[0])
          
        // }

            if((inputs.Username != undefined) && response.data.isSuccess){
                if((response.data.message==="Login Successfully")){
                    var y = JSON.stringify(response.data.data[0])
                    localStorage.setItem("loginUser", y);
                  
                    dispatch(Username(response.data.data[0]));
                    successMessage()
                    if(product.length>0){
                        navigate('/cartItem')
                    }
                    else{
                        navigate('/')
                    }
                }
               

            }
            else{
                alert("Login failed,Please check your username and password is correct.")
            }


            // ((inputs.Username != undefined) && response.data.isSuccess ? ((response.data.message==="Login Successfully")? (successMessage() ? ((product.length>0)? navigate('/cartItem'): navigate('/')) : errorMessage() ) )



            // ((inputs.Username != undefined) && response.data.isSuccess ? ((product.length>0)?navigate('/cartItem'): navigate('/')):alert("Login failed,Please check your username and password is correct."))

            // ((inputs.Username != undefined) && response.data.isSuccess ? successMessage() : errorMessage())
        });

       
    }

   

    const SignIn = () => {
       
        setLoinData(inputs);
        // console.log(inputs)
        loginApi(inputs);
        
     


    }

  
 

    return (
<div className='sgsd'>   
       
            <Container className='loginformcontainer' >    <Card  className="explore"> 
                <div className="  loginitem">
                <h1 className=" loginaccname">Login account</h1>
                    {/* {(typeof (loinData.Username) == "string") ? <div className="errormassg">Login failed,Please check your username and password is correct. </div> : " "} */}
                    <TextField
                        className="inputtextfield"
                        placeholder="Username"
                        label="Username"
                        variant="outlined"
                        name="Username"
                        onChange={handleChange}
                        value={inputs.Username}
                    />  <br/>
                    {/* { (typeof(inputs.firstname) != 'string') ? <span className="validatinn"> this name is not valid </span> : " "} */}
                    {(loinData.cheak == "demo") ?" " : ((loinData.Username == undefined) ? <span className="requ" >User Name is required </span> : " ")}
                   <br/> 
                    <TextField
                        className="inputtextfield"
                        id="outlined-password-input"
                        label="Password"
                        type="password"
                        autoComplete="current-password"
                        variant="outlined"
                        name="password"
                        onChange={handleChange}
                        value={inputs.password}
                    />  <br/>
                    {(loinData.cheak == "demo") ? " " : ((loinData.password == undefined) ? <span className="requ" > Password is required </span> : " ")}
                    {/* {(loinData.cheak!="demo") ? <span className="requ"> required</span> :' ' } */}
                    {/* { (typeof(inputs.password) != 'string') ? <span className="validatinn"> this name is not valid </span> : " "} */}

                    <br />  <br/> <br/>


                    {/* <Link to="/" className="nav">    <Button variant="contained" onClick={SignIn} className="button"  > Log in  </Button> </Link>   */}

                   


                    {/* <Button variant="contained" onClick={SignIn} className="button"  > Log in  </Button> */}
                    <span className="">  Don't Have accout </span> <Link to="/SignUp" className=""> SignUp</Link> <br/> <br/> 

                    <Button variant="contained" onClick={SignIn} className="loginbutton" > Log in  </Button>
                    <br/> <br/> <br/> <br/> <br/> 
                </div>


            </Card>   </Container>

      

            </div>

    )
}

export default Login