import React from "react";
import './web.css'
import { useEffect, useState } from "react";
import CircularProgress from '@mui/material/CircularProgress';
import 'animate.css';
import img1 from '../../assets/images/logo.png'
import { Link } from "react-router-dom";
import axios from "axios";
import { useSelector, useDispatch } from "react-redux";
import Username from '../../Redux/Actions/Action';
import Button from '@mui/material/Button';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import MenuIcon from '@mui/icons-material/Menu';
import Badge from '@mui/material/Badge';
import { styled } from '@mui/material/styles';
import IconButton from '@mui/material/IconButton';
import { Menu, MenuItem, Typography } from "@material-ui/core";
import NestedMenuItem from "material-ui-nested-menu-item";
import SearchBar from "material-ui-search-bar";

const StyledBadge = styled(Badge)(({ theme }) => ({
    '& .MuiBadge-badge': {
        right: -3,
        top: -3,
        border: `2px solid ${theme.palette.background.paper}`,
        padding: '0 4px',
    },
}));

export default function Header(prop) {


    const [menuPosition, setMenuPosition] = useState(null);
    const [toggleMenu, setToggleMenu] = useState(true)
    const [category, setCategory] = useState([]);
    const [cartdata, setCartdata] = useState()
    const [subcategory, setSubcategory] = useState([]);
    const [iscategory, setIscategory] = useState(false);
    const [searchItem, setSearchItem] = useState("")



    const dispatch = useDispatch()
    useEffect(() => {
        var dataa = localStorage.getItem("loginUser")
        dataa = JSON.parse(dataa)
        dispatch(Username(dataa))
    }, [])

    const { productdata, data } = useSelector((state) => {

        return ({
            productdata: state.productdata.addToCart,
            data: state.data.loginUser
        });
    });
console.log("data",data)
    const handleRightClick = (event) => {
        if (menuPosition) {
            return;
        }
        event.preventDefault();
        setMenuPosition({
            top: event.pageY,
            left: event.pageX
        });
    };

    const handleItemClick = (event) => {
        setMenuPosition(null);
    };


    useEffect(() => {

        axios.get("http://localhost:4900/productCategory/get").then((response) => {
            console.log('cat',response.data.data)
            setCategory(response.data.data)

            setIscategory(true)



        })
        // (data!=undefined && data.firstName!=undefined && data) ? setUserName(data):setUserName(localdata)



        axios.get("http://localhost:4900/SubCategory/GetSubCategoryList").then((response) => {


            setSubcategory(response.data.data)
            console.log('sub',response.data.data)
        })
    }, [])








    const [anchorEl, setAnchorEl] = React.useState(null);
    const open = Boolean(anchorEl);
    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        localStorage.removeItem("loginUser")
        setAnchorEl(null);
        dispatch(Username())
    };


    const buttonfunction = () => {
        (toggleMenu) ? setToggleMenu(false) : setToggleMenu(true)

    }
    const click = () => {
        setToggleMenu(true)
    }
    const search = (event) => {
       
       
        setSearchItem(event)

       

    }
    const requestSearch = () => {
        console.log("demo",searchItem)
    }
    return (

        <nav>

            <ul className="list" onClick={click}>

                {/* <li className="items">   <SearchBar
                    value={searchItem}
                    onChange={search}
                    onRequestSearch={requestSearch}
                   />
                 
                 </li> */}
                {( data!=undefined && data.roll == "admin" )?  <li className="items">  <Link to="/HomeDashboard" className="itemlogo" ><img className="logo1" src={img1} /></Link>  </li> :  <li className="items">  <Link to="/" className="itemlogo" ><img className="logo1" src={img1} /></Link>  </li>}
              
                {(toggleMenu) ? <>
                    <li className="items"> <Link to="/" className="">  <spacing className="nav">Home</spacing> </Link></li>
                    <li className="items"><Link to="/product" className="">  <span className="nav">All Product</span> </Link></li>





                    <li className="items" onClick={handleRightClick} >  <Typography className="allCategory"> All Category</Typography>
                        {!iscategory ? <CircularProgress /> : <Menu
                            open={!!menuPosition}
                            onClose={() => setMenuPosition(null)}
                            anchorReference="anchorPosition"
                            anchorPosition={menuPosition}
                        >


                            {category.map((val) => {
                                return (<> {(subcategory.filter((value) => (value.catId == val.catId))).length >= 1 ? <NestedMenuItem
                                    label={val.categoryName}
                                    parentMenuOpen={!!menuPosition}
                                    onClick={handleItemClick}
                                >
                                    {subcategory.map((data) => {
                                        return (
                                            <>
                                                {(val.catId == data.catId) ?
                                                    <MenuItem onClick={handleItemClick}>    {data.subCategoryName} </MenuItem>


                                                    : ""}
                                            </>
                                        )
                                    })}
                                </NestedMenuItem> : <MenuItem onClick={handleItemClick}>
                                    {val.categoryName}
                                    {val.catId}
                                </MenuItem>}

                                </>
                                )
                            })}



                        </Menu>}

                    </li>

                    <li className="items"> {(data != undefined && data.first_name != undefined) ? (<> <span className="logname nameee"
                        onClick={handleClick}>
                        {data.first_name + data.last_name}
                    </span>     <Menu
                        anchorEl={anchorEl}
                        open={open} >

                            <MenuItem onClick={handleClose} className="loout">Logout</MenuItem>
                        </Menu>
                    </>) : <Link to="/SignIn" className="login" > <Button className="nav">Log in</Button>   </Link>}
                    </li>
                    <li className="items" >  <Link to="/cartItem" className="loginnn" >  <IconButton aria-label="cart">
                        <StyledBadge badgeContent={(productdata) ? productdata.length : 0} color="secondary">
                            <ShoppingCartIcon className="nav" />
                        </StyledBadge>
                    </IconButton>  </Link>   </li>
                </> : " "}
            </ul>
            <MenuIcon className="btn" onClick={buttonfunction}></MenuIcon>

        </nav>
    )

}

