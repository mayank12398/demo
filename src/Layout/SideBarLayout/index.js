import Header from './Header'
import Footer from './Footer';
import React from 'react';
import SideNav from './SideNav';


const SideBarLayout = (props)=>{
    return (<>
    <Header />
    <SideNav/>
  {props.children}
   <Footer />
    
    </>);
 }
 
 export default SideBarLayout;