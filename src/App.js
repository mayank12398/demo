import React from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import BaseLayout from "./Layout/BaseLayout/Layout";
import Login from "./component/userComponent/Login";
import SignUp from './component/userComponent/SignUp';
import Product from './component/userComponent/Product'
import Content from './component/userComponent/Content'
import SideBarLayout from "./Layout/SideBarLayout";
import Category from "./component/adminComponent/category";
import CategoryFo from './component/adminComponent/categoryForm'
import SubCategoryFo from './component/adminComponent/subcategoryForm'
import AddnewCategoryFo from "./component/adminComponent/Addnewcategory";
import HomepageDashbord from "./component/adminComponent/HomepageDashbord";
import SubCategory from "./component/adminComponent/subCtegory";
import AddnewSubCategoryFo from "./component/adminComponent/AddNewSubcategory";
import { useSelector } from "react-redux";
import ProductApi from './component/adminComponent/ProductApi'
import AddNewProduct from './component/adminComponent/ProductAddNew'
import ShowProduct from "./component/userComponent/ProductShow";
import CartItems from "./component/userComponent/cartItem";
import PaypalPayment from "./component/userComponent/Paypal";
import UpdateProductItem from './component/adminComponent/updateProductItem'

export default function App(props) {
    const data = useSelector((state) => {
        return (state.data.loginUser);
    });

    return (

        <>

            <Router>
                <Routes>
                    {(data == undefined && data == null) ? <Route path="/SignIn" exact={true} element={<BaseLayout >
                        <Login />
                    </BaseLayout>}

                    /> : <> <Route path="/SignIn" exact={true} element={<BaseLayout>
                        <Content />
                    </BaseLayout>} />

                    </>}

                    {(data == undefined && data == null) ? <Route path="/SignUp" exact={true} element={<BaseLayout >
                        <SignUp />
                    </BaseLayout>}

                    /> : <> <Route path="/SignUp" exact={true} element={<BaseLayout>
                        <Content />
                    </BaseLayout>} />

                    </>}

                    <Route path="/product" exact={true} element={<BaseLayout >
                        <Product />
                    </BaseLayout>}

                    />


                    <Route path="/productshow" exact={true} element={<BaseLayout >
                        <ShowProduct />
                    </BaseLayout>}

                    />

                    <Route path="/cartItem" exact={true} element={<BaseLayout >
                        <CartItems />
                    </BaseLayout>}

                    />
                    <Route path="/product/cartItem" exact={true} element={<BaseLayout >
                        <CartItems />
                    </BaseLayout>}

                    />
                    <Route path="/" exact={true} element={<BaseLayout >
                        <Content />
                    </BaseLayout>}

                    />

                    <Route path="/Paymentmode" exact={true} element={
                        <PaypalPayment />
                    }

                    />


                    <Route path="/category" exact={true} element={<SideBarLayout >
                        <Category />
                    </SideBarLayout>}

                    />

                    <Route path={`/categoryForm/:id`} exact={true} element={<SideBarLayout >
                        <CategoryFo />
                    </SideBarLayout>}

                    />

                    <Route path="/AddSubcategory" exact={true} element={<SideBarLayout >
                        <SubCategory />
                    </SideBarLayout>}

                    />

                    <Route path={`/subCategoryForm/:id`} exact={true} element={<SideBarLayout >
                        <SubCategoryFo />
                    </SideBarLayout>}

                    />

                    <Route path="/addnewcategoryForm" exact={true} element={<SideBarLayout >
                        <AddnewCategoryFo />
                    </SideBarLayout>}

                    />

                    <Route path="/addnewsubCategoryForm" exact={true} element={<SideBarLayout >
                        <AddnewSubCategoryFo />
                    </SideBarLayout>}
                    />

                    <Route path="/UpdateProductItem/:id" exact={true} element={<SideBarLayout >
                        <UpdateProductItem />
                    </SideBarLayout>}
                    />



                    {/* {(data != undefined && data != null) ? <Route path="/HomeDashboard" exact={true} element={<SideBarLayout >
                        <HomepageDashbord />
                    </SideBarLayout>}

                    /> : <>  <Route path="/HomeDashboard" exact={true} element={<BaseLayout >
                        <Content />
                    </BaseLayout>} />
                    </>} */}

                    <Route path="/HomeDashboard" exact={true} element={<SideBarLayout >
                        <HomepageDashbord />
                    </SideBarLayout>}

                    />

                    <Route path="/Products" exact={true} element={<SideBarLayout >
                        <ProductApi />
                    </SideBarLayout>}

                    />

                    <Route path="/ProductsForm" exact={true} element={<SideBarLayout >
                        <AddNewProduct />
                    </SideBarLayout>}

                    />

                </Routes>
            </Router>








        </>

    )

}